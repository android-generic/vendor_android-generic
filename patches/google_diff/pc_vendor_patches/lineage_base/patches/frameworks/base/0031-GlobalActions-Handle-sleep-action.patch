From 701d651051269264d09eec43518d1ed6dfa52db6 Mon Sep 17 00:00:00 2001
From: saranya <saranya.gopal@intel.com>
Date: Tue, 17 Apr 2018 15:52:08 +0530
Subject: [PATCH] GlobalActions: Handle 'sleep' action

Some Intel platforms do not provide separate
events for power key press and release. This
makes it impossible to detect long press of
power button. So, the solution is to handle
only short press and add 'sleep' also as an
option in GlobalActions menu. This patch
handles 'sleep' option.

Change-Id: Iaae59b324e5ba6eaed9e507fdaa8e5006535716c
Tracked-On: OAM-56502
Signed-off-by: saranya <saranya.gopal@intel.com>
Signed-off-by: Madhusudhan S <madhusudhan.s@intel.com>

Conflicts:
	packages/SystemUI/src/com/android/systemui/globalactions/GlobalActionsDialog.java

Conflicts:
	packages/SystemUI/src/com/android/systemui/globalactions/GlobalActionsDialog.java

Conflicts:
	core/res/res/values-zh-rHK/strings.xml
	packages/SystemUI/src/com/android/systemui/globalactions/GlobalActionsDialog.java

Conflicts:
	packages/SystemUI/src/com/android/systemui/globalactions/GlobalActionsDialog.java
---
 core/res/res/values/config.xml                |  1 +
 core/res/res/values/strings.xml               |  3 ++
 core/res/res/values/symbols.xml               |  1 +
 .../globalactions/GlobalActionsDialog.java    | 36 +++++++++++++++++++
 4 files changed, 41 insertions(+)

diff --git a/core/res/res/values/config.xml b/core/res/res/values/config.xml
index 46f2fdeb5e5..b9d5fff87f2 100644
--- a/core/res/res/values/config.xml
+++ b/core/res/res/values/config.xml
@@ -2773,6 +2773,7 @@
         <item>lockdown</item>
         <item>power</item>
         <item>restart</item>
+        <item>sleep</item>
         <item>logout</item>
         <item>screenshot</item>
         <item>bugreport</item>
diff --git a/core/res/res/values/strings.xml b/core/res/res/values/strings.xml
index 9a7e80fb6f5..8dbb93a0c4a 100644
--- a/core/res/res/values/strings.xml
+++ b/core/res/res/values/strings.xml
@@ -560,6 +560,9 @@
 
     <!-- label for item that restarts phone in phone options dialog [CHAR LIMIT=24]-->
     <string name="global_action_restart">Restart</string>
+    
+    <!-- label for item that puts device to sleep in phone options dialog [CHAR LIMIT=24]-->
+    <string name="global_action_sleep">Sleep</string>
 
     <!-- label for item that opens emergency features in the phone options dialog [CHAR LIMIT=24]-->
     <string name="global_action_emergency">Emergency</string>
diff --git a/core/res/res/values/symbols.xml b/core/res/res/values/symbols.xml
index 2d8d0d06f75..d3df48baaf8 100644
--- a/core/res/res/values/symbols.xml
+++ b/core/res/res/values/symbols.xml
@@ -1787,6 +1787,7 @@
   <java-symbol type="string" name="global_action_power_off" />
   <java-symbol type="string" name="global_action_power_options" />
   <java-symbol type="string" name="global_action_restart" />
+  <java-symbol type="string" name="global_action_sleep" />
   <java-symbol type="string" name="global_actions_airplane_mode_off_status" />
   <java-symbol type="string" name="global_actions_airplane_mode_on_status" />
   <java-symbol type="string" name="global_actions_toggle_airplane_mode" />
diff --git a/packages/SystemUI/src/com/android/systemui/globalactions/GlobalActionsDialog.java b/packages/SystemUI/src/com/android/systemui/globalactions/GlobalActionsDialog.java
index 016ad45f7d7..c6a95d707e6 100644
--- a/packages/SystemUI/src/com/android/systemui/globalactions/GlobalActionsDialog.java
+++ b/packages/SystemUI/src/com/android/systemui/globalactions/GlobalActionsDialog.java
@@ -60,7 +60,9 @@ import android.os.Binder;
 import android.os.Handler;
 import android.os.IBinder;
 import android.os.Message;
+import android.os.PowerManager;
 import android.os.RemoteException;
+import android.os.SystemClock;
 import android.os.SystemProperties;
 import android.os.UserHandle;
 import android.os.UserManager;
@@ -184,6 +186,7 @@ public class GlobalActionsDialog implements DialogInterface.OnDismissListener,
     private static final String GLOBAL_ACTION_KEY_LOGOUT = "logout";
     static final String GLOBAL_ACTION_KEY_EMERGENCY = "emergency";
     static final String GLOBAL_ACTION_KEY_SCREENSHOT = "screenshot";
+    static final String GLOBAL_ACTION_KEY_SLEEP = "sleep";
 
     public static final String PREFS_CONTROLS_SEEDING_COMPLETED = "SeedingCompleted";
     public static final String PREFS_CONTROLS_FILE = "controls_prefs";
@@ -645,6 +648,8 @@ public class GlobalActionsDialog implements DialogInterface.OnDismissListener,
                 addIfShouldShowAction(tempActions, getAssistAction());
             } else if (GLOBAL_ACTION_KEY_RESTART.equals(actionKey)) {
                 addIfShouldShowAction(tempActions, restartAction);
+            } else if (GLOBAL_ACTION_KEY_SLEEP.equals(actionKey)) {
+				addIfShouldShowAction(tempActions, new SleepAction());
             } else if (GLOBAL_ACTION_KEY_SCREENSHOT.equals(actionKey)) {
                 addIfShouldShowAction(tempActions, new ScreenshotAction());
             } else if (GLOBAL_ACTION_KEY_LOGOUT.equals(actionKey)) {
@@ -888,6 +893,37 @@ public class GlobalActionsDialog implements DialogInterface.OnDismissListener,
             return true;
         }
     }
+    
+    private final class SleepAction extends SinglePressAction implements LongPressAction { 
+        private SleepAction() { 
+            super(R.drawable.ic_restart, R.string.global_action_sleep); 
+        } 
+ 
+        @Override 
+        public boolean onLongPress() { 
+            PowerManager mPowerManager = (PowerManager) 
+                   mContext.getSystemService(Context.POWER_SERVICE); 
+            mPowerManager.goToSleep(SystemClock.uptimeMillis()); 
+            return true; 
+        } 
+ 
+        @Override 
+        public boolean showDuringKeyguard() { 
+            return true; 
+        } 
+ 
+        @Override 
+        public boolean showBeforeProvisioning() { 
+            return true; 
+        } 
+ 
+        @Override 
+        public void onPress() { 
+            PowerManager mPowerManager = (PowerManager) 
+                   mContext.getSystemService(Context.POWER_SERVICE); 
+            mPowerManager.goToSleep(SystemClock.uptimeMillis()); 
+        } 
+    } 
 
     private class EmergencyAffordanceAction extends EmergencyAction {
         EmergencyAffordanceAction() {
-- 
2.17.1

